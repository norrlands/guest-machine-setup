.PHONY: all clean

all: README.pdf

%.xml: % Makefile
	asciidoc -b docbook -d article -v $<
	printf "/<articleinfo>/+1a\n    <edition>$$(git describe --tags || echo unknown)</edition>\n.\nwq!\n" | ex $@

%.pdf: %.xml
	 a2x -v --dblatex-opts "-P latex.output.revhistory=0 -P doc.toc.show=0" -f pdf $<

clean:
	rm -f README.pdf README.xml
